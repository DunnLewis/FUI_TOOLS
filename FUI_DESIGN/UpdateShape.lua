------------------------------------------------------------------------------

------------------------------------------------------------------------------


-----------------------------------------------------
-- trim(strTrim)
-- 
-- returns strTrim with leading and trailing spaces 
-- removed.
-----------------------------------------------------
function trim(strTrim)
	strTrim = string.gsub(strTrim, "^(%s+)", "") -- remove leading spaces
	strTrim = string.gsub(strTrim, "$(%s+)", "") -- remove trailing spaces
	return strTrim
end



------------------------------------------------------------------------------
-- SETUP ---------------------------------------------------------------------
------------------------------------------------------------------------------
INFOTXT = "This script will load vertex data from a .dfsh file."
PIXELOPTIONS = {"Pixels (zero is bottom/left)", "Pixels (zero is top/left)", "Pixels (zero is image center)", "Fusion (0.5 is image center)"}
PATHOPTIONS = {"XY Path", "PolyLine Path"}


--[[
if not tool then
	tool = composition:GetAttrs().COMPH_ActiveTool
	if not tool then
		print("This is a tool script, you must select a tool in the flow to run this script")
		composition:GetFrameList()[1]:SwitchMainView('ConsoleView') 
		exit()
	end
end

--]]

------------------------------------------------------------------------------
-- MAIN ----------------------------------------------------------------------
------------------------------------------------------------------------------

fileName = nil
-- build a table of controls that can be animated (Points or Numbers)
controls = {}
controlNames = {}


--]]

while fileHandle == nil do
	-- ask the user what text file to process
	dialog = {{"infotxt", Name = "", "Text", Default = INFOTXT, Wrap = true, Lines = 2, ReadOnly = true}}
	if ret == nil then
		table.insert(dialog, {"filename", "FileBrowse", Default = ""})
	else
		table.insert(dialog, {"filename", "FileBrowse", Default = ret.filename})
	end
	
	if err then
		table.insert(dialog, {"warning", Name = "Warning", "Text", Default = err, Wrap = true, Lines = 4, ReadOnly = true})
	end
	
	ret = composition:AskUser("Select Polygon Shape (*.dfsh)", dialog)
	if ret == nil then return end
	
	-- can we open the file?
	fileName = MapPath(ret.filename)
	fileHandle, err = io.open(fileName, "r")
end


-- read the first few lines so we can display a preview in the next dialog.
firstDataLine = nil
secondDataLine = nil
i = 0
preview = ""
New_List=""
local customShapeName="";
string.gsub(fileName, "^.+[/\\](.-)$", function(s) customShapeName= s:match("(.+)%..+") end)


--print(customShapeName)
array ={}

line = fileHandle:read("*l")

	--print( "lineNumber:", line)
	---------------------------------------START CHECKER

while line and (i < 500) do
	-- look for first line that starts with a number (in case we have to skip some initial lines)
	-- and try to detect the number of columns per line
	if firstDataLine == nil and string.find(line, "^%s*-?%d+") then
		firstDataLine = i
		detectColumns = 0
		string.gsub(trim(line), "([^%s,;]+)", function(v) detectColumns = detectColumns + 1 end)
		--print(i,"detected columns in 1st line:", detectColumns)

	


		if detectColumns >= 4 then

	
		string.gsub(trim(line), "([^%s,;]+)", function(v) detectColumns = detectColumns + 1 end)
		print ("detectColumns More:",detectColumns)
else


		--string.gsub(trim(line), "([^%s,;]+)", function(v) detectColumns = detectColumns + 1 end)
		print ("detectColumns less:",detectColumns)
    end


end
	

    local t = {} 

    

    for num in string.gmatch(line, "(-?%d*%.?%d+)") do

        table.insert(t, num)

        a = tonumber(num)
        a=a*1000
		
		New_List = New_List .. a ..","
     
    end
    table.insert(array, t)

	

	
	--preview = preview .."BOB={{".. line .. "}},\n "
	preview = customShapeName.." = {{".. New_List .. "}},\n"



	
	
	line = fileHandle:read("*l")
	i = i + 1

end

---------------------------------------END CHECKER

fileHandle:close()
if i == 500 and line ~= nil then
	preview = preview .. "(...)\n"
end
if firstDataLine == nil then
   print("This file doesn't seem to have any data that can be imported.")
   composition:GetFrameList()[1]:SwitchMainView('ConsoleView') 
   exit()
end
-- get filename for text label
string.gsub(fileName, "^.+[/\\](.-)$", function(s) fileNameLabel = s end)





	










-- set up defaults
ret = {}
ret.skip = firstDataLine
ret.control = 1
if detectColumns == 1 then
	-- If only one column has been detected, default input should be "angle" (if available).
	ret.control = eyeon.get_table_index(controlNames, "Angle") or 1
else
	-- If two or more columns have been detected, default input should be "center" (if available).
	ret.control = eyeon.get_table_index(controlNames, "Center (2D Point)") or 1
end


-- show preview dialog. remember: dropdown indices are zero based while LUA tables start with an index of 1.
ret = composition:AskUser("Import Animation", {
	{"preview", Name = "Preview of "..fileNameLabel, "Text", Default = preview, Wrap = true, Lines = 6, ReadOnly = true},

	{"SHAPEFILE", "FileBrowse", Default = ""}
	})



shapeName = nil

if ret == nil then return end

	shapeName = MapPath(ret.SHAPEFILE)



-- start reading file for real
fileHandle, err = io.open(fileName, "a")
if fileHandle == nil then
   print(err)
--fileHandle, err = io.open(shapeName, "a")
   exit()
end


 fileShape, errx = io.open(shapeName, "a")
if fileShape == nil then
   print(errx)
--fileHandle, err = io.open(shapeName, "a")
   exit()
end
 



fileShape:write(preview)
fileShape:close()
fileShape = nil


fileHandle:close()
fileHandle = nil


--print("Imported "..#values.." keys to "..tool:GetAttrs().TOOLS_Name..":"..theControl:GetAttrs().INPS_Name)

composition:EndUndo(true)
if hold then composition.UpdateMode = hold end

